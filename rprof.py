"""
The goal of this script is to generate some general information
about the isolated galaxy on the MSU super computer. It creates
a radial 1d profile for each of the fields in `fields`.
"""

import os.path
import yt
from params import (fields, widths, weights, logs, unit_dict,
                    max_r, rprof_cpf, c, rprof_annotate_time)

from utils import get_extrema, get_dfiles, make_file_name, process_data

yt.enable_parallelism()

extrema = get_extrema()
extrema['spherical_radius'] = (0, max_r[0])


d_series = yt.DatasetSeries(get_dfiles(), setup_function=process_data,
                            parallel=rprof_cpf)

for ds in d_series.piter():
    for w in widths.keys():
        sp = ds.sphere(c, widths[w])
        for field in fields:
            for weight in weights.keys():
                fn = make_file_name('1dProfsr', field, w, ds, weight=weight)
                if os.path.isfile(fn):
                    pass
                else:
                    plogs = {}
                    plogs[field[1]] = logs[field[1]]
                    p = yt.ProfilePlot(sp, 'radius', field,
                                       weight_field=weights[weight],
                                       x_log=False,
                                       y_log=plogs)
                    p.set_unit('radius', 'kpc')
                    p.set_unit(field[1], unit_dict[field[1]])
                    p.set_ylim(field, extrema[field[1]][0],
                               extrema[field[1]][1])
                    if rprof_annotate_time:
                        p.plots[field].axes.set_title("Time = %d Myr" % int(
                            ds.current_time.in_units('Myr')))
                        p.plots[field].axes.title.set_fontproperties(
                        p._font_properties)
                    p.save(fn)

quit()
