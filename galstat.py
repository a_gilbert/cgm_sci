import pickle
import yt
import math
from params import (r_vir, c, r_values, fields, unit_dict, galstat_cpf,
                    stat_file)
from utils import get_dfiles, process_data

"""Generates statistical information over time for spherical shells generated
from information in params and the `get_spherical_shells` function.
"""

yt.enable_parallelism()


@yt.particle_filter(requires=["creation_time"], filtered_type='all')
def formed_star(pfilter, data):
    filter = data["all", "creation_time"] > 0
    return filter


def get_stat_dict(total_quantity, volume_weighted_avg, vol_weighted_var,
                  vmin, vmax):
    out = {}
    out['total'] = total_quantity
    out['volw_avg'] = volume_weighted_avg
    out['volw_stddev'] = math.sqrt(vol_weighted_var)
    out['min'] = vmin
    out['max'] = vmax
    return out


def get_field_stats(ds, field_list, unit_dict):
    out = {}
    for field in field_list:
        units = unit_dict[field[1]]
        weight = ('gas', 'cell_volume')
        total_q = ds.quantities.total_quantity(field)
        volw_avg = ds.quantities.weighted_average_quantity(field, weight)
        vol_var = ds.quantities.weighted_variance(field, weight)
        qmin, qmax = ds.quantities.extrema(field)
        total_q = float(total_q.in_units(units))
        volw_avg = float(volw_avg.in_units(units))
        vol_var = float(vol_var[0])
        qmin = float(qmin.in_units(units))
        qmax = float(qmax.in_units(units))
        out[field[1]] = get_stat_dict(total_q, volw_avg, vol_var, qmin, qmax)
    return out


def get_spherical_shells(ds, c, r_values, r_vir):
    shells = {}
    r_values = [r_vir[0] * i for i in r_values]
    r_units = len(r_values)*[r_vir[1]]
    r_values = list(zip(r_values, r_units))
    for i in range(len(r_values)):
        n = 'r%d' % i
        if i == 0:
            sp = ds.sphere(c, r_values[i])
        else:
            sp1 = ds.sphere(c, r_values[i-1])
            sp2 = ds.sphere(c, r_values[i])
            sp = sp2 - sp1
        shells[n] = sp
    shells['rtot'] = ds.sphere(c, r_values[-1])
    return shells


def get_shell_names(r_values):
    n = []
    for i in range(len(r_values)):
        n.append('r%d' % i)
    n.append('rtot')
    return n


dseries = yt.DatasetSeries(get_dfiles(), parallel=galstat_cpf,
                           setup_function=process_data)
storage = {}
for sto, ds in dseries.piter(storage=storage):
    ds.add_particle_filter('formed_star')
    shells = get_spherical_shells(ds, c, r_values, r_vir)
    out = {}
    for k in shells.keys():
        out[k] = get_field_stats(shells[k], fields, unit_dict)
    if ds.particles_exist:
        out['smass'] = shells['rtot']['formed_star',
                                      'particle_mass'].in_units('Msun').sum()
    else:
        out['smass'] = yt.YTQuantity(0, 'Msun')
    out['time'] = ds.current_time.in_units('yr')
    sto.result = out
    sto.result_id = str(ds)

if yt.is_root():
    time_dep_data = {}
    time_dep_data['units'] = unit_dict
    time_dep_data['time'] = []
    time_dep_data['smass'] = []
    time_dep_data['regs'] = {}

    for r in get_shell_names(r_values):
        d = time_dep_data['regs']
        d[r] = {}
        for field in fields:
            d[r][field[1]] = {}
            for stat in get_stat_dict(0, 0, 0, 0, 0).keys():
                d[r][field[1]][stat] = []

    for k in sorted(iter(storage)):
        time_dep_data['time'].append(float(storage[k]['time']))
        time_dep_data['smass'].append(float(storage[k]['smass']))
        for r in get_shell_names(r_values):
            for field in fields:
                for stat in get_stat_dict(0, 0, 0, 0, 0).keys():
                    time_dep_data['regs'][r][field[1]][stat].append(
                        float(storage[k][r][field[1]][stat]))

    pickle.dump(time_dep_data, open(stat_file, 'wb'),
                protocol=pickle.HIGHEST_PROTOCOL)
