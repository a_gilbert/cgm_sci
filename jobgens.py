"""Classes used to generate job submission scripts """
import math


class ScriptMaker(object):
    """A basic class for generating a job submission script on an hpc system.

    This is a skeleton object that is capable of generating an hpcc job
    execution file based on the input parameters. By using set and get
    methods and it can be used to generate multiple execution files.

    Parameters
    ----------
    fn : string
        The name of the file to save the job script to.
    nodes : int
        Number of nodes to request for the job.
    cores : int
        Number of cores to request for the job.
    walltime : list of int
        A list with three elements, the first representing hours,
        the second minutes, and the third seconds. 
    name : string
        Name of the job.
    join : boolean
        Whether or not to join the error and output.
    keep : boolean
        Whether or not to keep the error and output.
    out
    mail : boolean
        Whether or not to enable the script to mail at
        certain points.
    mail_settings : string
        Settings for mailing.
    address : string
        Where to send the mail.

    """

    def __init__(self, fn, nodes, cores, walltime, name, join, keep, mail,
                 mail_settings, address):
        self.fn = fn
        self.nodes = nodes
        self.cores = cores
        self.walltime = walltime
        self.name = name
        self.join = join
        self.keep = keep
        self.mail = mail
        self.mail_settings = mail_settings
        self.address = address
        self.commands = []

    def set_fn(self, new_fn):
        self.fn = new_fn

    def set_nodes(self, new_nodes):
        self.nodes = new_nodes

    def set_core(self, new_cores):
        self.cores = new_cores

    def set_walltime(self, new_walltime):
        self.walltime = new_walltime

    def set_name(self, new_name):
        self.name = new_name

    def set_join(self, new_join):
        self.join = new_join

    def set_keep(self, new_keep):
        self.keep = new_keep

    def set_mail(self, new_mail):
        self.mail = new_mail

    def set_mail_settings(self, new_mail_settings):
        self.mail_settings = new_mail_settings

    def set_address(self, new_address):
        self.address = new_address

    def set_commands(self, new_commands):
        self.commands = new_commands

    def add_command(self, command):
        self.commands.append(command)

    def make_script(self):
        f = open(self.fn, 'w')
        f.write("#!/bin/bash\n")
        f.write("#PBS -N %s\n" % self.name)
        f.write("#PBS -l nodes=%d:ppn=%d\n" % (self.nodes, self.cores))
        f.write("#PBS -l walltime=%02d:%02d:%02d\n" % (self.walltime[0],
                                                       self.walltime[1],
                                                       self.walltime[2]))
        f.write("#PBS -e $PBS_O_WORKDIR/%s.ER\n" % self.name)
        f.write("#PBS -o $PBS_O_WORKDIR/%s.OU\n" % self.name)
        if self.join:
            f.write("#PBS -j oe\n")
        if self.keep:
            f.write("#PBS -k oe\n")
        if self.mail:
            f.write("#PBS -m %s\n" % self.mail_settings)
            f.write("#PBS -M %s\n" % self.address)
        for c in self.commands:
            f.write("%s\n" % c)
        f.close()


class MsuScriptMaker(ScriptMaker):

    def __init__(self, fn, nodes, cores, walltime, name, join, keep, mail,
                 mail_settings, address, mem):
        super(MsuScriptMaker, self).__init__(fn, nodes, cores, walltime, name,
                                             join, keep, mail, mail_settings,
                                             address)
        self.add_command("#PBS -l mem=%dgb" % mem)


class BwScriptMaker(ScriptMaker):

    def __init__(self, fn,  nodes, cores, walltime, name, join, keep, mail,
                 mail_settings, address):
        super(BwScriptMaker, self).__init__(fn, nodes, cores, walltime, name,
                                            join, keep, mail, mail_settings,
                                            address)
        self.add_command("module swap PrgEnv-cray PrgEnv-gnu")
        self.add_command("export LD_LIBRARY_PATH=$HOME/software/anaconda3/" +
                         "lib:$LD_LIBRARY_PATH")
        self.add_command("source activate")
        self.add_command("cd $PBS_O_WORKDIR")

    def get_num_cores(self, flist, cpf):
        return len(flist)*cpf

    def get_num_nodes(self, flist, cpf, tcpn=32):
        nc = self.get_num_cores(flist, cpf)
        return math.ceil(nc/float(tcpn))

    def get_cores_p_node(self, flist, cpf):
        cond = self.get_num_nodes(flist, cpf)
        if cond > 1:
            return 32
        elif cond == 1:
            return self.get_num_cores(flist, cpf)


class BwSliceMaker(BwScriptMaker):

    def __init__(self, cpf, walltime, join, keep, mail,
                 mail_settings, address, source_files):
        fn = "run_slicer.sh"
        name = "slicer"
        c = self.get_num_cores(source_files, cpf)
        n = self.get_num_nodes(source_files, cpf)
        cpn = self.get_cores_p_node(source_files, cpf)
        super(BwSliceMaker, self).__init__(fn, n, cpn, walltime,
                                           name, join, keep, mail,
                                           mail_settings, address)
        self.add_command("aprun -n %d python slicer.py" % c)
        self.add_command('exit 0')
        self.make_script()


class BwProjMaker(BwScriptMaker):

    def __init__(self, cpf, walltime, join, keep, mail,
                 mail_settings, address, source_files):
        fn = "run_projector.sh"
        name = "projector"
        c = self.get_num_cores(source_files, cpf)
        n = self.get_num_nodes(source_files, cpf)
        cpn = self.get_cores_p_node(source_files, cpf)
        super(BwProjMaker, self).__init__(fn, n, cpn, walltime,
                                          name, join, keep, mail,
                                          mail_settings, address)
        self.add_command("aprun -n %d python projector.py" % c)
        self.add_command('exit 0')
        self.make_script()


class BwRProfMaker(BwScriptMaker):
    def __init__(self, cpf, walltime, join, keep, mail,
                 mail_settings, address, source_files):
        fn = "run_rprof.sh"
        name = "rprof"
        c = self.get_num_cores(source_files, cpf)
        n = self.get_num_nodes(source_files, cpf)
        cpn = self.get_cores_p_node(source_files, cpf)
        super(BwRProfMaker, self).__init__(fn, n, cpn, walltime,
                                           name, join, keep, mail,
                                           mail_settings, address)
        self.add_command("aprun -n %d python rprof.py" % c)
        self.add_command('exit 0')
        self.make_script()


class BwHProfMaker(BwScriptMaker):
    def __init__(self, cpf, walltime, join, keep, mail,
                 mail_settings, address, source_files):
        fn = "run_hprof.sh"
        name = "hprof"
        c = self.get_num_cores(source_files, cpf)
        n = self.get_num_nodes(source_files, cpf)
        cpn = self.get_cores_p_node(source_files, cpf)
        super(BwHProfMaker, self).__init__(fn, n, cpn, walltime,
                                           name, join, keep, mail,
                                           mail_settings, address)
        self.add_command("aprun -n %d python hprof.py" % c)
        self.add_command('exit 0')
        self.make_script()


class BwProf2dMaker(BwScriptMaker):
    def __init__(self, cpf, walltime, join, keep, mail,
                 mail_settings, address, source_files):
        fn = "run_prof2d.sh"
        name = "prof2d"
        c = self.get_num_cores(source_files, cpf)
        n = self.get_num_nodes(source_files, cpf)
        cpn = self.get_cores_p_node(source_files, cpf)
        super(BwProf2dMaker, self).__init__(fn, n, cpn, walltime,
                                            name, join, keep, mail,
                                            mail_settings, address)
        self.add_command("aprun -n %d python prof2d.py" % c)
        self.add_command('exit_0')
        self.make_script()


class BwPhase3dMaker(BwScriptMaker):
    def __init__(self, cpf, walltime, join, keep, mail,
                 mail_settings, address, source_files):
        fn = "run_phase3d.sh"
        name = "phase3d"
        c = self.get_num_cores(source_files, cpf)
        n = self.get_num_nodes(source_files, cpf)
        cpn = self.get_cores_p_node(source_files, cpf)
        super(BwPhase3dMaker, self).__init__(fn, n, cpn, walltime,
                                             name, join, keep, mail,
                                             mail_settings, address)
        self.add_command("aprun -n %d python phase3d.py" % c)
        self.add_command('exit_0')
        self.make_script()
