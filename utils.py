import glob
import yt
import numpy as np
import pickle
from params import (d_files, extrema_file, combo_file, pbase_names)


def process_data(ds):
    len_u = ds.parameters['LengthUnits']
    mass_u = ds.parameters['MassUnits']
    time_u = ds.parameters['TimeUnits']
    disk_position = yt.YTArray(ds.parameters['DiskGravityPosition']*len_u,
                               'cm')
    disk_angular_momentum = yt.YTArray(
        ds.parameters['DiskGravityAngularMomentum'], 'dimensionless')
    disk_mass = yt.YTQuantity(ds.parameters['DiskGravityStellarDiskMass'],
                              'Msun').in_units('g')
    disk_radius = yt.YTQuantity(ds.parameters[
        'DiskGravityStellarDiskScaleHeightR'], 'Mpc').in_units('cm')
    disk_height = yt.YTQuantity(ds.parameters[
        'DiskGravityStellarDiskScaleHeightz'], 'Mpc').in_units('cm')
    bulge_mass = yt.YTQuantity(ds.parameters['DiskGravityStellarBulgeMass'],
                               'Msun').in_units('g')
    bulge_radius = yt.YTQuantity(ds.parameters['DiskGravityStellarBulgeR'],
                                 'Mpc').in_units('cm')
    disk_dm_radius = yt.YTQuantity(ds.parameters[
        'DiskGravityDarkMatterR'], 'Mpc').in_units('cm')
    disk_dm_dens = yt.YTQuantity(ds.parameters['DiskGravityDarkMatterDensity'],
                                 'g/cm**3')
    grav_const = yt.YTQuantity(6.67259e-8, 'cm**3/g/s/s')

    def _diskdeltax(field, data):
        x_disk_position = disk_position[0]
        return data['x'] - x_disk_position

    def _diskdeltay(field, data):
        y_disk_position = disk_position[1]
        return data['y'] - y_disk_position

    def _diskdeltaz(field, data):
        z_disk_position = disk_position[2]
        return data['z'] - z_disk_position

    def _diskcylzmag(field, data):
        lx = disk_angular_momentum[0]
        ly = disk_angular_momentum[1]
        lz = disk_angular_momentum[2]
        return lx*data['disk_delta_x'] + ly*data['disk_delta_y'] + lz*data['disk_delta_z']

    def _diskx(field, data):
        lx = disk_angular_momentum[0]
        return data['disk_delta_x'] - data['disk_cylindrical_z_mag']*lx

    def _disky(field, data):
        ly = disk_angular_momentum[1]
        return data['disk_delta_y'] - data['disk_cylindrical_z_mag']*ly

    def _diskz(field, data):
        lz = disk_angular_momentum[2]
        return data['disk_delta_z'] - data['disk_cylindrical_z_mag']*lz

    def _disksphrad(field, data):
        return np.sqrt(data['disk_x']*data['disk_x'] +
                       data['disk_y']*data['disk_y'] +
                       data['disk_z']*data['disk_z'] +
                       data['disk_cylindrical_z_mag']*data[
                           'disk_cylindrical_z_mag'])

    def _diskcylr(field, data):
        return np.sqrt(data['disk_x']*data['disk_x'] +
                       data['disk_y']*data['disk_y'] +
                       data['disk_z']*data['disk_z'])

    def _accelsph(field, data):
        g = grav_const
        mb = bulge_mass
        rb = bulge_radius
        ddm = disk_dm_dens
        rdm = disk_dm_radius
        out = g*mb/((data['disk_spherical_r']+rb)**2)
        out += (np.pi*g*ddm*(rdm**3)/(data['disk_spherical_r']**2))*(-2.0*np.arctan(
            data['disk_spherical_r']/rdm) + 2.0*np.log(1.0 + (data['disk_spherical_r']/rdm)) +
                                                                     np.log(1.0 + (data['disk_spherical_r']/rdm)**2))
        out = abs(out)/data['disk_spherical_r']
        return out

    def _accelcylr(field, data):
        g = grav_const
        md = disk_mass
        dr = disk_radius
        dz = disk_height**2
        h = 'disk_cylindrical_z_mag'
        out = g*md*data['disk_cylindrical_r']
        out = out/np.sqrt(((data['disk_cylindrical_r']**2) + ((dr + np.sqrt((data[h]**2) + dz))**2))**3)
        out = abs(out)/data['disk_cylindrical_r']
        return out

    def _accelcylz(field, data):
        g = grav_const
        md = disk_mass
        dr = disk_radius
        dz = disk_height
        r = 'disk_cylindrical_r'
        h = 'disk_cylindrical_z_mag'
        out = g*md/np.sqrt((data[h]**2)+(dz**2))
        out *= data[h]/(np.sqrt(((data[r]**2)+((dr + np.sqrt((data[h]**2) + (dz**2)))**2))**3))
        out *= (dr + np.sqrt((data[h]**2)+(dz**2)))
        out = abs(out)*(data[h]/abs(data[h]))
        return out

    def _accx(field, data):
        lx = disk_angular_momentum[0]
        out = data['disk_spherical_acc'] * data['disk_delta_x']
        out += data['disk_cylindrical_r_acc'] * data['disk_x']
        out += data['disk_cylindrical_z_acc'] * lx
        return out

    def _accy(field, data):
        ly = disk_angular_momentum[1]
        out = data['disk_spherical_acc'] * data['disk_delta_y']
        out += data['disk_cylindrical_r_acc'] * data['disk_y']
        out += data['disk_cylindrical_z_acc'] * ly
        return out

    def _accz(field, data):
        lz = disk_angular_momentum[2]
        out = data['disk_spherical_acc']*data['disk_delta_z']
        out += data['disk_cylindrical_r_acc']*data['disk_z']
        out += data['disk_cylindrical_z_acc']*lz
        return out

    def _accmag(field, data):
        return np.sqrt(data['acc_x']**2 + data['acc_y']**2 + data['acc_z']**2)

    def _tff(field, data):
        return np.sqrt((2*data['disk_spherical_r'])/data['acc_mag'])

    def _voitt(field, data):
        return data['cooling_time']/data['disk_ff']

    ds.add_field('disk_delta_x', function=_diskdeltax, units='cm')
    ds.add_field('disk_delta_y', function=_diskdeltay, units='cm')
    ds.add_field('disk_delta_z', function=_diskdeltaz, units='cm')
    ds.add_field('disk_cylindrical_z_mag', function=_diskcylzmag, units='cm')
    ds.add_field('disk_x', function=_diskx, units="cm")
    ds.add_field('disk_y', function=_disky, units='cm')
    ds.add_field('disk_z', function=_diskz, units='cm')
    ds.add_field('disk_spherical_r', function=_disksphrad, units='cm')
    ds.add_field('disk_cylindrical_r', function=_diskcylr, units='cm')
    ds.add_field('disk_spherical_acc', function=_accelsph, units='s**(-2)')
    ds.add_field('disk_cylindrical_r_acc', function=_accelcylr,
                 units='s**(-2)')
    ds.add_field('disk_cylindrical_z_acc', function=_accelcylz, units='cm/s/s')
    ds.add_field('acc_x', function=_accx, units='cm/s/s')
    ds.add_field('acc_y', function=_accy, units='cm/s/s')
    ds.add_field('acc_z', function=_accz, units='cm/s/s')
    ds.add_field('acc_mag', function=_accmag, units='cm/s/s')
    ds.add_field('disk_ff', function=_tff, units='s')
    ds.add_field(('gas', 'pmeasure'), function=_voitt, units='dimensionless',
                 take_log=False)
    return ds


def get_dfiles():
    return glob.glob(d_files)


def get_extrema():
    extrema = pickle.Unpickler(open(extrema_file, 'rb'))
    extrema = extrema.load()
    return extrema


def get_combos():
    combos = pickle.Unpickler(open(combo_file, 'rb'))
    combos = combos.load()
    return combos


def make_file_name(base_key, field, width, ds, direction=None,
                   weight=None, field2=None, pfield=None):
    name = pbase_names[base_key] + '/' + width + '/'
    if pfield is not None:
        name = name + pfield[1] + '/'
    name = name + field[1] + '/'
    if field2 is None:
        pass
    else:
        name = name + field2[1] + '/'
    if direction is None:
        if weight is None:
            pass
        else:
            name = name + weight + '/'
    else:
        if weight is None:
            name = name + direction + '/'
        else:
            name = name + direction + '/' + weight + '/'
    name = name + str(ds) + '.png'
    return name
