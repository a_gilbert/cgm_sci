#!/bin/bash
#PBS -l nodes=1:ppn=4
#PBS -l walltime=04:00:00
#PBS -N movie_maker
#PBS -e $PBS_O_WORKDIR/slicer.err
#PBS -o $PBS_O_WORKDIR/slicer.out
#PBS -m abe
#PBS -M agilbert39@gatech.edu

module swap PrgEnv-cray PrgEnv-gnu
export LD_LIBRARY_PATH=$HOME/software/anaconda3/lib:$LD_LIBRARY_PATH

source activate

cd $PBS_O_WORKDIR

aprun -n 4 python moviemaker.py

exit 0
