import matplotlib
matplotlib.use('Agg')
import matplotlib.pyplot as plt
import math
from mpl_toolkits.mplot3d import axes3d
from mpl_toolkits.axes_grid1 import AxesGrid
import numpy as np
import os
from scipy.signal import correlate
import pickle
from params import sym_dict as sd
from params import comparison_reg as cr
from params import (unit_labels, galcor_make_directories, galcor_write_extrema,
                    c, r_vir, r_values, pbase_names, extrema_file, stat_file,
                    fields, y_scales)
from utils import get_dfiles
import yt

plt.rc('text', usetex=False)


def normalize(varr):
    """Return varr normalized between 0 and 1."""
    varr = varr / math.sqrt(np.dot(varr, varr))
    return varr


def get_sfr(smass, time):
    """Gets the first order numerical derivative of the values in
    smass."""
    smass = np.array(smass)
    sfr = np.ediff1d(smass) / np.ediff1d(time)
    sfr = np.insert(sfr, [0], 0)
    return sfr


def get_dtime(smass, cmass, time):
    sfr = get_sfr(smass, time)
    return cmass/sfr


def basic_plt(ax, x, y, xlabel, ylabel, title, **kwargs):
    """Make a basic plot on a 2d axis.

    Parameters
    ----------
    ax : matplotlib.axes object
    x : array_like
        Values of the function along the x axis.
    y : array_like
        Values of the function along the y axis.
    xlabel : string
        Label for the x-axis.
    ylabel : string
        Label for the y-axis.
    titls : string
        Title for the axes.
    """
    ax.plot(x, y, **kwargs)
    ax.set_xlabel(xlabel)
    ax.set_ylabel(ylabel)
    ax.set_title(title, fontsize=10)


def total_plt(ax, field, unit, ftot, time,
              time_unit="($yr$)"):
    """Plot the total value of some quantity.

    Parameters
    ----------
    ax : matplotlib.axes object
        Axes on which to plot values.
    field : string
        What field is being plotted
    unit : string
        The units of the field.
    ftot : list of float
        Total value of field of interest.
    time : list of float
        Time values of ftot.
    time_unit : string, optional
        What units the time is in.
    """
    xl = r"$t$ %s" % time_unit
    yl = r"$\Sigma %s$ %s" % (field, unit)
    basic_plt(ax, time, ftot, xl, yl, "")


def ncor_plot(ax, field1, fval1, field2, fval2, cross=True):
    """Plots the normalized correlation of field1 with field2.

    Parameters
    ----------
    ax : matplotlib.axes
        The axes on which the plot will be made.
    field1 : string
        Field being evaluated
    fval1 : list of float
        Values of field1 being correlated.
    field2 : string
        Note that this is the value whose index is shifted!
    fval2 : list of float
        Values of field 2 being correlated.
    tstep : float
        Amount of time between each index in fval and smass.
    cross : boolean, optional
        Whether to write that the plot is a cross or auto correlation.
        """
    yl = r"Correlation"
    xl = r"$\delta t$"
    if cross:
        s = "Cross"
    else:
        s = "Auto"
    title = r"%s Correlation of $%s$ with $%s$" % (s, field1,
                                                   field2)
    fval1 = normalize(np.array(fval1))
    fval2 = normalize(np.array(fval2))
    corr = correlate(fval1, fval2)
    delt_t = np.arange(-len(fval1) + 1, len(fval1))
    basic_plt(ax, delt_t, corr, xl, yl, title)


def tot_sfr_ncor(ax, field, fval, smass, time):
    """Plot the correlation of field with the star formation rate.

    Parameters
    ----------
    ax : matplotlib.axes
        Where the plot is drawn.
    field : string
        The field that the sfr is being correlated against.
    fval : list of float
        Values of the field.
    smass : list of float
        Values of the stellar mass.
    time : list of float
        The time interval between each data dump.
    """
    field = r"\Sigma %s" % field
    sfr = get_sfr(smass, time)
    ncor_plot(ax, r"\dot{M}_{\ast}", sfr, field, fval)


def mean_sfr_ncor(ax, field, fval, smass, tstep):
    """Generates a correlation plot of fval with the assumption that fval is
    the list of mean values for the field in question.

    Parameters
    ----------
    ax : matplotlib.axes object
        Where the plot will be drawn.
    field : string
       The name of the field being plotted.
    fval : list of float
        Values of the field being checked for correlation.
    smass : list of float
        The total mass of stars in the sim at a given time.
    tstep : float
        The amount of time between each index value of the above lists."""
    field = r"\langle %s \rangle" % field
    sfr = get_sfr(smass, tstep)
    ncor_plot(ax, r"\dot{M}_{\ast}", sfr, field, fval)


def auto_ncor(ax, field, fval, tstep):
    """Generates the auto correlation plot of fval.

    Parameters
    ----------
    ax : matplotlib.axes object
        Where the plot will be drawn
    field : string
        The name of the field being plotted.
    fval : list of float
        Values of the field.
    tstep : float
        The amount of time between each index value of the
        fval list.
    """
    ncor_plot(ax, field, fval, field, fval, cross=False)


def mean_auto_ncor(ax, field, fval, tstep):
    """Generate the auto correlation plot for the mean value of a quantity.

    Parameters
    ----------
    ax : matplotlib.axes object
        Where the plot will be drawn.
    field : string
        The name of the field being plotted
    fval : list of float
        Values of the field.
    tstep : float
        The amount of time between each index value of the fval list.
    """
    field = r"\langle %s \rangle" % field
    auto_ncor(ax, field, fval, tstep)


def tot_auto_ncor(ax, field, fval, tstep):
    """Generate the auto correlation plot for the total value of a quantity.

    Parameters
    ----------
    ax : matplotlib.axes object
        Where the plot will be drawn.
    field : string
        The name of the field being plotted.
    fval : list of float
        Values of the field.
    tstep : float
        The amount of time between each index value of the fval list.
    """
    field = r"\Sigma %s" % field
    auto_ncor(ax, field, fval, tstep)


def avg_plot(ax, field, unit, favg, fdev, t, t_unit, y_scale, **kwargs):
    """Generate a plot of the average and standard deviation of the field.

    Because of the large range of scales in the max and min values of these
    fields, this function has an extremely high number of sampling points. It
    also defaults to only lines that move forward in time rather than a
    complete wire frame.

    Parameters
    ----------
    ax : matplotlib.axes object
        Where the plot is going to be put.
    field : string
        The name of the field being plotted
    unit : string
        Units the field values are in.
    favg : list of float
        The list of average values at every unit of time.
    fdev : list of float
        List of deviations at every unit of time.
    fmax : list of float
        List of max values at every unit of time.
    fmin : list of float
        List of min values at every unit of time.
    t : List of float
        Time step values for each index of all other lists.
    t_unit : string
        Units for time.
    y_scale : string
        What scale to put the y axis in.
    """
    ax.set_ylabel(r"$%s$ %s" % (field, unit))
    ax.set_xlabel(r"$t$ %s" % t_unit)
    l1 = r"$%s$ avg" % field
    l2 = r"$%s$ stddev" % field
    ax.plot(t, favg, 'b', label=l1)
    ax.plot(t, fdev, 'g', label=l2)
    ax.set_yscale(y_scale)


def rel_plt(ax, field, rnorm, rsample, fnorm_val, fsample_val, t, t_unit):
    """Generate a plot that shows sample values at every time
    divided by some normalizing sample to show how it deviates
    from that normalizing sample.

    Parameters
    ----------
    ax : matplotlib.axes object
        Where the plot will be drawn.
    field : string
        The field being plotted.
    rnorm : string
        A name for the region from which the norm is being used.
    rsample : string
        A name for the region from which the sample is being examined.
    fnorm_val : list of float
        The values from the normalizing sample at every unit of time.
    fsample_val : list of float
        The values from the sample of interest at every unit of time.
    t : list of float
        Time stamps for the corresponding indices in the other lists.
    t_unit : string
        The units time is being measured in. If you are feeling cheeky, you
        could justifiably make it meters, just be sure to multiple t by
        the speed of light.
        """
    fn = np.array(fnorm_val)
    fs = np.array(fsample_val)
    y = fs / fn
    xl = r"$t$ %s" % t_unit
    yl = r"$%s(%s)/%s(%s)$" % (field, rsample, field, rnorm)
    title = r"Relative value of $%s$" % field
    basic_plt(ax, t, y, xl, yl, title)


def rel_tot_plt(ax, field, rnorm, rsample, fnorm, fsample, t, t_unit):
    """Generate a plot of the total value of the field in the volume of interest
    relative to the same total value in another volume of interest.

    Parameters
    ----------
    ax : matplotlib.axes object
        Where the plot is drawn.
    field : string
        The field being normalized
    rnorm : string
        String name of the normalizing region
    rsample : string
        String name of the sample region
    fnorm : list of float
        field data from `rnorm`.
    fsample : list of float
        field data from `rsample`.
    t : list of float
        The time of each data sample
    t_unit : string
        Units of time."""
    field = r"\Sigma %s" % field
    rel_plt(ax, field, rnorm, rsample, fnorm, fsample, t, t_unit)


def rel_mean_plt(ax, field, rnorm, rsample, fnorm, fsample, t, t_unit):
    """Plot the ratio of the volume weighted mean field value of one region to
    another region.

    Parameters
    ----------
    ax : matplotlib.axes object
        Where the plot is drawn.
    field : string
        The field being normalized
    rnorm : string
        String name of the normalizing region
    rsample : string
        String name of the sample region
    fnorm : list of float
        field data from `rnorm`.
    fsample : list of float
        field data from `rsample`.
    t : list of float
        The time of each data sample
    t_unit : string
        Units of time."""
    field = r"\langle %s \rangle" % field
    rel_plt(ax, field, rnorm, rsample, fnorm, fsample, t, t_unit)


def rel_dev_plt(ax, field, rnorm, rsample, fnorm, fsample, t, t_unit):
    """Plot the ratio of the volume weighted deviation of the field of one
    region to another region.

    Parameters
    ----------
    ax : matplotlib.axes object
        Where the plot is drawn.
    field : string
        The field being normalized
    rnorm : string
        String name of the normalizing region
    rsample : string
        String name of the sample region
    fnorm : list of float
        field data from `rnorm`.
    fsample : list of float
        field data from `rsample`.
    t : list of float
        The time of each data sample
    t_unit : string
        Units of time."""
    field = r"\sigma_{%s}" % field
    rel_plt(ax, field, rnorm, rsample, fnorm, fsample, t, t_unit)


def make_binfo_plt(region, sregion, field, unit, ftot, sftot, fvolw_avg,
                   sfvolw_avg, fvolw_stddev, sfvolw_stddev, fmin, fmax,
                   smass, time, fn, y_scale, myr=True):
    """A function to generate a figure with nine subplots arranged
    in a 3 by 3 grid.

    The plots will be of
    1) The total value of the field in the region
    2) The total values cross correlation with the star formation rate.
    3) The total values auto correlation.
    4) A 3d plot of the normal distribution in time.
    5) The cross correlation of the mean with the star formation rate.
    6) Autocorrelation of the mean.
    7) Fraction of the total in the region vs. the total of the super
       region.
    8) The relative mean, mean of the region divided by the mean of the
       entire region.
    9) Relative variance to the entire super region.

    Parameters
    ----------
    region : string
        A string labeling which region of a spherical shell is being
        investigated.
    sregion : string
        A string labeling the region of a spherical shell that region
        is compared against.
    field : string
        The name of the field being investigated
    unit : string
        What units to put in the axis label for the field.
    ftot : list of float
        A list containing the total value of the field in the region at every
        time.
    sftot : list of float
        ftot for the region containing the sub region of interest.
    fvolw_avg : list of float
        List containing the volume weighted average of the field at every time.
    sfvolw_avg : list of float
        fvolw_avg for the region containing the one being examined.
    fvolw_stddev : list of float
        List of the standard deviation at every time for the field.
    sfvolw_stddev : list of float
        fvolw_stddev for the super region.
    fmin : list of float
        List of minimum values for the field.
    fmax : list of float
        List of maximum values for the field.
    smass : list of float
        List of total stellar mass in the simulation.
    time : list of float
        List of times of each value in other lists in units of years.
    phase : string
        What phase the gas is in.
    fn : string
        What file to save the figure in.
    myr : Boolean, optional
        Whether to put time in units of millions of years or not.
        """
    time = np.array(time)
    if myr:
        time = time * 1e-6
        t_u = "$(Myr)$"
    else:
        t_u = "$(yr)$"
    plt.close('all')
    fig = plt.figure(figsize=(12, 12))
    tot_ax = fig.add_subplot(3, 3, 1)
    tot_cc_ax = fig.add_subplot(3, 3, 2)
    tot_ac_ax = fig.add_subplot(3, 3, 3)
    avg_ax = fig.add_subplot(3, 3, 4)
    mean_cc_ax = fig.add_subplot(3, 3, 5)
    mean_ac_ax = fig.add_subplot(3, 3, 6)
    rel_tot_ax = fig.add_subplot(3, 3, 7)
    rel_mean_ax = fig.add_subplot(3, 3, 8)
    rel_dev_ax = fig.add_subplot(3, 3, 9)
    fig.suptitle(r'Basic Information on $%s$ in region %s' %
                 (field, region))
    total_plt(tot_ax, field, unit, ftot, time, time_unit=t_u)
    tot_sfr_ncor(tot_cc_ax, field, ftot, smass, time)
    tot_auto_ncor(tot_ac_ax, field, ftot, time)
    avg_plot(avg_ax, field, unit, fvolw_avg, fvolw_stddev, time, t_u, y_scale)
    mean_sfr_ncor(mean_cc_ax, field, fvolw_avg, smass, time)
    mean_auto_ncor(mean_ac_ax, field, fvolw_avg, time)
    rel_tot_plt(rel_tot_ax, field, sregion, region, sftot, ftot, time, t_u)
    rel_mean_plt(rel_mean_ax, field, sregion, region, sfvolw_avg, fvolw_avg,
                 time, t_u)
    rel_dev_plt(rel_dev_ax, field, sregion, region, sfvolw_stddev,
                fvolw_stddev, time, t_u)
    fig.subplots_adjust(wspace=0.5, hspace=0.5)
    plt.savefig(fn)


def plt_stellar_info(smass, time, fn1, fn2, my=True):
    """Given the total stellar mass and the time of each measurement,
    generate a plot of the total mass and the star formation rate of
    the simulation.

    Parameters
    ----------
    smass : list of float
        The stellar mass at every data dump in units of solar masses.
    time : list of float
        The time of each data dump in years.
    fn1 : string
        Where to save the total stellar mass plot.
    fn2 : string
        Where to save the star formation rate plot.
    my : boolean, optional
        Whether to make the plot in units of millions of years"""
    plt.close('all')
    time = np.array(time)
    fig = plt.figure()
    plt.plot(time, smass)
    plt.ylabel(r'Total Stallar Mass ($M_{\odot}$)')
    if my:
        plt.xlabel(r'Time ($Myr$)')
    else:
        plt.xlabel(r'Time ($yr$)')
    plt.tight_layout()
    plt.savefig(fn1)
    fig2 = plt.figure()
    sfr = get_sfr(smass, time)
    if my:
        plt.semilogy(time * 1e-6, sfr)
    else:
        plt.semilogy(time, sfr)
    plt.ylabel(r'SFR ($M_{\odot} yr^{-1}$)')
    if my:
        plt.xlabel(r'Time ($Myr$)')
    else:
        plt.xlabel(r'Time ($yr$)')
    plt.tight_layout()
    plt.savefig(fn2)


def shell_plot(rn, fn, rv=r_values, rvir=r_vir, c=c):
    """Make a plot showing the regions of the simulation being sampled.

    Parameters
    ----------
    rn : list of string
        The name of all the regions in the data set.
    fn : string
        What file to save the figure under.
    rv : list of float, optional
        Radii of the regions examined in units of rvir.
    rvir : tuple, optional
        The virial radius.
    c : tuple, optional
        The center of the plot.

    Notes
    -----
    This module and galstat module are designed to work with rvir to be any
    arbitrary radius, but that radius must be used consistently (aka only
    defined in the params module.)"""
    d_list = get_dfiles()
    sample = d_list[0]
    ds = yt.load(sample)
    s = yt.SlicePlot(ds, 'x', 'density')
    for i in range(len(rn)-1):
        s.annotate_sphere(c, radius=(rv[i]*rvir[0], rvir[1]))
        y = ds.arr([0, rv[i]*rvir[0], 0], rvir[1]).in_units('code_length')
        p = ds.arr(c, 'code_length') + y
        s.annotate_text(p, rn[i])
    s.save(fn)


def write_avg_extrema(fn, rtot_dict):
    """Write the average extrema values for each field into a pickle file.

    Parameters
    ----------
    fn : string
        The name of the file to dump the data into.
    rtot_dict : dictionary
        Dictionary containing the max and min values at every time
        for every field of interest."""
    avg_extrema = {}
    for field in rtot_dict.keys():
        avg_extrema[field] = (min(rtot_dict[field]['min']),
                              max(rtot_dict[field]['max']))
    pickle.dump(avg_extrema, open(fn, 'wb'),
                protocol=pickle.HIGHEST_PROTOCOL)


def main():
    data = pickle.Unpickler(open(stat_file, 'rb'))
    data = data.load()

    if galcor_make_directories:
        for x in data['regs'].keys():
                path = pbase_names['stats'] + '/' + x
                os.makedirs(path)
    if galcor_write_extrema:
        write_avg_extrema(extrema_file, data['regs']['rtot'])
    shell_plot(list(sorted(data['regs'].keys())), './plots/stats/shells.png')
    plt_stellar_info(data['smass'], data['time'], './plots/stats/smass.png',
                     './plots/stats/sfr.png')
    for r in data['regs'].keys():
        d1 = data['regs'][r]
        d2 = data['regs'][cr]
        for f in fields:
            k = f[1]
            fn = pbase_names['stats'] + '/%s/%s_binfo.png' % (r, k)
            make_binfo_plt(r, cr, sd[k], unit_labels[k],
                           d1[k]['total'], d2[k]['total'],
                           d1[k]['volw_avg'], d2[k]['volw_avg'],
                           d1[k]['volw_stddev'], d2[k]['volw_stddev'],
                           d1[k]['min'], d1[k]['max'], data['smass'],
                           data['time'], fn,
                           y_scales[k])


if __name__ == "__main__":
    main()
