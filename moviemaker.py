import os
from subprocess import call, run


my_mod = "1"


def get_movie_name(path, custom_mod=None):
    terms = path.split('/')
    terms = terms[2: len(terms)]
    name = terms[0]
    for x in range(1, len(terms)):
        name = name + "_" + terms[x]
    if custom_mod is None:
        name = name + ".mp4"
    else:
        name = name + "_" + custom_mod + ".mp4"
    return name


bottom_dirs = []

ptypes = ['profs', 'projs', 'slices']

for p in ptypes:
    for path, dirs, files in os.walk('./plots/%s' % p, topdown=True):
        if len(dirs) == 0 and len(files) > 0:

            bottom_dirs.append(path)

home = os.getcwd()
for path in bottom_dirs:
    os.chdir(path)
    m_name = get_movie_name(path, custom_mod=my_mod)
    new_m_name = "%s/plots/%s" % (home, m_name)
    command1 = "movie " + m_name + " 15 *.png"
    call(['/bin/bash', '-i', '-c', command1])
    run(['mv', m_name, new_m_name])
    os.chdir(home)
