#!/bin/bash
#PBS -l nodes=20:ppn=32
#PBS -l walltime=24:30:00
#PBS -N gal_stat
#PBS -e $PBS_O_WORKDIR/gal_stat.err
#PBS -o $PBS_O_WORKDIR/gal_stat.out
#PBS -m abe
#PBS -M agilbert39@gatech.edu

module swap PrgEnv-cray PrgEnv-gnu
export LD_LIBRARY_PATH=$HOME/software/anaconda3/lib:$LD_LIBRARY_PATH

source activate

cd $PBS_O_WORKDIR

aprun -n 640 python galstat.py

exit 0
