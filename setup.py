"""Generates the phase3d combinations, directory hierarchy that plots are
saved to, constructs the job batch files for the run, and then submits
those files.."""

from itertools import combinations
import os
from jobgens import (BwSliceMaker, BwProjMaker, BwHProfMaker, BwRProfMaker,
                     BwPhase3dMaker, BwProf2dMaker)
from params import (pbase_names, widths, fields, directions,
                    prof_fields, fields2, phase3d_add_radius, combo_file,
                    slicer_cpf, slicer_walltime, projector_cpf,
                    projector_walltime, prof2d_cpf, prof2d_walltime,
                    rprof_cpf, rprof_walltime, hprof_cpf, hprof_walltime,
                    phase3d_cpf, phase3d_walltime, join, keep, mail,
                    mail_settings, address, weights, make_slices,
                    make_projections, make_hprof, make_rprof, make_prof2d,
                    make_phase3d)
from utils import get_dfiles
import pickle

# Create and write the phase3d combinations.
if phase3d_add_radius:
    phase3d_fields = fields
    phase3d_fields.append(('index', 'spherical_radius'))
combo_out = []
for c in combinations(phase3d_fields, 3):
    combo_out.append(c)
pickle.dump(combo_out, open(combo_file, 'wb'),
            protocol=pickle.HIGHEST_PROTOCOL)


# Create the file hierarchy, except for stats.
for name in pbase_names.keys():
    if name == 'slice':
        for width in widths.keys():
            for field in fields:
                for coord in directions:
                    path = pbase_names[name] + '/' + width + '/' + field[1] +\
                           '/' + coord
                    os.makedirs(path)
    if name == 'projs':
        for width in widths.keys():
            for field in fields:
                for coord in directions:
                    for weight in weights.keys():
                        path = pbase_names[name] + '/' + width + '/' +\
                               field[1] + '/' + coord + '/' + weight
                        os.makedirs(path)
    if name == '1dProfsr':
        for width in widths.keys():
            for field in fields:
                for weight in weights.keys():
                    path = pbase_names[name] + '/' + width + '/' + field[1] +\
                           '/' + weight
                    os.makedirs(path)
    if name == '1dProfsh':
        for width in widths.keys():
            for field in fields:
                for weight in weights.keys():
                    path = pbase_names[name] + '/' + width + '/' + field[1] +\
                           '/' + weight
                    os.makedirs(path)
    if name == '2dProfs':
        for width in widths.keys():
            for pfield in prof_fields:
                for field in fields:
                    for field2 in fields2:
                        for weight in weights.keys():
                            path = pbase_names[name] + '/' + width + '/'
                            path += pfield[1] + '/' + field[1] + '/'
                            path += field2[1] + '/' + weight
                            os.makedirs(path)
    if name == '3dProfs':
        for i in range(len(combo_out)):
            for f in prof_fields:
                path = pbase_names[name] + '/' + 'combo%d/%s' % (i, f[1])
                os.makedirs(path)

# Create the execution scripts for slicing, projecting, phase3d, hprof, rprof,
# and prof2d
f = get_dfiles()
if make_slices:
    BwSliceMaker(slicer_cpf, slicer_walltime, join, keep, mail,
                 mail_settings, address, f)
if make_projections:
    BwProjMaker(projector_cpf, projector_walltime, join, keep, mail,
                mail_settings, address, f)
if make_hprof:
    BwHProfMaker(hprof_cpf, hprof_walltime, join, keep, mail,
                 mail_settings, address, f)
if make_rprof:
    BwRProfMaker(rprof_cpf, rprof_walltime, join, keep, mail,
                 mail_settings, address, f)
if make_prof2d:
    BwProf2dMaker(prof2d_cpf, prof2d_walltime, join, keep, mail,
                  mail_settings, address, f)
if make_phase3d:
    BwPhase3dMaker(phase3d_cpf, phase3d_walltime, join, keep, mail,
                   mail_settings, address, f)

quit()
