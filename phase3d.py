import matplotlib
matplotlib.use('Agg')
import matplotlib.pyplot as plt
import matplotlib.colors as clr
import matplotlib.ticker as mticker
from mpl_toolkits.mplot3d import Axes3D
import numpy as np
import os.path
import yt
from params import (cmaps, prof_fields, res, theta, phi, max_r, c,
                    logs, unit_dict, unit_labels, phase3d_add_radius,
                    phase3d_cpf, phase3d_annotate_time, pfield_weights)
from utils import get_combos, get_dfiles, get_extrema, process_data

yt.enable_parallelism()

my_extremas = get_extrema()
if phase3d_add_radius:
    my_extremas['spherical_radius'] = (0, max_r[0])


def get_3d_prof(ids, x_field, y_field, z_field, d_field, res=res,
                units=unit_dict, extrema=my_extremas, logs=logs,
                weight=None, c=c):
    """Generates all of the point values for a 3d phase scatter plot
    and returns the points in a dict.

    Parameters
    ----------
    ids : yt.frontend obj
        The data obj to get the data from.
    x_field : str tuple
        The xfield of the profile.
    y_field : str tuple
        Yfield of the profile. This is not passed to yt!
    z_field : str tuple
        Zfield of the profile.
    d_field : str_tuple
        This is the field binned by yt.
    res : int
        The number of bins in the x, y, and z directions.
    units : dict
        Dictionary of units to use for all values, keys are the
        first indices of the fields.
    extrema : dict
        Upper and lower limits for all fields, passed to yt. Keys
        work just like in `units`
    logs : dict
        A dictionary of booleans on whether or not to make field values
        logarithmic.
    weight : None, optional
        What field to weight the profile bins by, defaults to none.
    c : tuple, optional
        Where to put the center of the data object.

    Returns
    -------
    prof : yt profile object
        """
    k_extrema = {}
    k_logs = {}
    k_units = {}
    for f in [x_field, y_field, z_field, d_field]:
        k_extrema[f] = extrema[f[1]]
        k_logs[f] = logs[f[1]]
        k_units[f] = units[f[1]]
    ad = ids.all_data()
    prof = yt.create_profile(ad, [x_field, y_field, z_field],
                             fields=[d_field], n_bins=[res, res, res],
                             extrema=k_extrema, logs=k_logs, units=k_units,
                             weight_field=weight)
    out = [prof.x, prof.y, prof.z,
           prof.field_data[d_field].in_units(units[d_field[1]])]
    return out


def process_3d_prof(profd, x_field, y_field, z_field, d_field,
                    units=unit_dict):
    out = {'x_field': x_field, 'y_field': y_field, 'z_field': z_field,
           'd_field': d_field, 'x_vec': [], 'y_vec': [], 'z_vec': [],
           'd_vec': []}
    d = profd[3]
    for i in range(0, res):
        for j in range(0, res):
            for k in range(0, res):
                if float(d[i, j, k]) != 0.0:
                    out['x_vec'].append(float(profd[0][i]))
                    out['y_vec'].append(float(profd[1][j]))
                    out['z_vec'].append(float(profd[2][k]))
                    out['d_vec'].append(float(d[i, j, k]))
    return out


def log_tick_formatter(val, pos=None):
    return "$10^{%s}$" % format(int(val))


def plot_3d_prof(x_vals, x_field, y_vals, y_field, z_vals, z_field,
                 d_vals, d_field, fn, extrema=my_extremas, theta=theta,
                 phi=phi, s=1.1, logs=logs, unit_labels=unit_labels,
                 cmap='arbre', title=None):
    """Creates a 3d scatter plot where points are colored according to
    d_vals.

    Parameters
    ----------
    x_vals : list
        List of values for the x value of each point.
    x_field : str tuple
        The field, and ultimately the label for the x-axis.
    y_vals : list
        Y values for the points on the scatter plot.
    y_field : str tuple
        Field and label for the y-axis.
    z_vals : list
        List of values for the z value of each point.
    z_field : str tuple
        Field and label for the z-axis.
    d_vals : list
        List for the colormap of each point.
    d_field : str tuple
        Field and label for the colorbar.
    fn : string
        File to save the plot under.
    extrema : dict, optional
        A list of max and min values for the axes.
    theta : float
        The elevation of the camera.
    phi : float
        The azimuthal angle of the camera.
    s : float, optional
        The size of all particles.
    logs : dict, optional
        A dict of booleans for whether to make a field logarithmic or not.
    unit_labels : dict, optional
        A dict with the unit labels for each field being plotted.
    cmap : string, optional
        The colormap of the particles.
    title : string, optional
        The title to put on the figure.
        """
    fig = plt.figure()
    ax = fig.add_subplot(111, projection='3d')
    ax.view_init(theta, phi)
    x_label = x_field[1] + unit_labels[x_field[1]]
    y_label = y_field[1] + unit_labels[y_field[1]]
    z_label = z_field[1] + unit_labels[z_field[1]]
    d_label = d_field[1] + unit_labels[d_field[1]]
    x_lims = [extrema[x_field[1]][0], extrema[x_field[1]][1]]
    y_lims = [extrema[y_field[1]][0], extrema[y_field[1]][1]]
    z_lims = [extrema[z_field[1]][0], extrema[z_field[1]][1]]
    d_lims = [extrema[d_field[1]][0], extrema[d_field[1]][1]]
    ax.set_xlim(x_lims)
    ax.set_ylim(y_lims)
    ax.set_zlim(z_lims)
    ax.set_xlabel(x_label)
    ax.set_ylabel(y_label)
    ax.set_zlabel(z_label)
    if title is not None:
        ax.set_title(title)
    if logs[x_field[1]]:
        x_lims = np.log10(x_lims)
        ax.set_xlim(x_lims)
        ax.xaxis.set_major_formatter(mticker.FuncFormatter(log_tick_formatter))
        x_vals = np.log10(x_vals)
    if logs[y_field[1]]:
        y_lims = np.log10(y_lims)
        y_vals = np.log10(y_vals)
        ax.set_ylim(y_lims)
        ax.yaxis.set_major_formatter(mticker.FuncFormatter(log_tick_formatter))
    if logs[z_field[1]]:
        z_lims = np.log10(z_lims)
        z_vals = np.log10(z_vals)
        ax.set_zlim(z_lims)
        ax.zaxis.set_major_formatter(mticker.FuncFormatter(log_tick_formatter))
    if logs[d_field[1]]:
        my_norm = clr.LogNorm(d_lims[0], d_lims[1])
    elif not logs[d_field[1]]:
        my_norm = clr.Normalize(d_lims[0], d_lims[1])

    c = ax.scatter(x_vals, y_vals, z_vals, s=s, c=d_vals, cmap=cmap,
                   norm=my_norm, marker='o', alpha=1, edgecolors='face')
    cb = plt.colorbar(c)
    cb.set_label(d_label)
    plt.setp(ax.get_xticklabels(), fontsize=8)
    plt.setp(ax.get_yticklabels(), fontsize=8)
    plt.setp(ax.get_zticklabels(), fontsize=8)
    plt.savefig(fn)
    plt.close('all')


d_series = yt.DatasetSeries(get_dfiles(), setup_function=process_data,
                            parallel=phase3d_cpf)

all_combos = get_combos()

for ds in d_series.piter():
    for i in range(len(all_combos)):
        c = 'combo%d' % i
        combo = all_combos[i]
        print("on combo %d" % i)
        for f in prof_fields:
            if phase3d_annotate_time:
                t = "Time = %d Myr" % int(ds.current_time.in_units('Myr'))
            else:
                t = None
            pfn = './plots/phase3d/%s/%s/%s.png' % (c, f[1], str(ds))
            if os.path.isfile(pfn):
                print('skipping')
            else:
                p = get_3d_prof(ds, combo[0], combo[1], combo[2], f,
                                weight=pfield_weights[f[1]])
                p = process_3d_prof(p, combo[0], combo[1], combo[2], f)
                pfn = './plots/phase3d/%s/%s/%s' % (c, f[1], str(ds))
                plot_3d_prof(p['x_vec'], p['x_field'],
                             p['y_vec'], p['y_field'],
                             p['z_vec'], p['z_field'],
                             p['d_vec'], p['d_field'],
                             pfn, title=t, cmap=cmaps[f[1]])
quit()
