"""
The goal of this script is to generate some general information
about the isolated galaxy on the MSU super computer. It creates
a 2d projection plot of `fields` at each key value of the
dictionary `widths` with respect to the fields in `fields2`.


For the profiles, each version of the plot is also
generated with a number of different weights, which are given by the
key values of the dictionary `weights`.
"""
import os.path
import yt
from params import (fields, fields2, prof_fields, widths, weights,
                    prof2d_annotate_time, logs, unit_dict, max_r,
                    prof2d_cpf, cmaps, c)
from utils import process_data, get_extrema, get_dfiles, make_file_name

yt.enable_parallelism()

extrema = get_extrema()
extrema['spherical_radius'] = (0, max_r[0])

d_series = yt.DatasetSeries(get_dfiles(), setup_function=process_data,
                            parallel=prof2d_cpf)

for ds in d_series.piter():
    for w in widths.keys():
        sp = ds.sphere(c, widths[w])
        for pfield in prof_fields:
            for field in fields:
                for field2 in fields2:
                    for weight in weights.keys():
                        fn = make_file_name('2dProfs', field, w, ds,
                                            weight=weight, field2=field2,
                                            pfield=pfield)
                        if os.path.isfile(fn):
                            pass
                        else:
                            p = yt.PhasePlot(sp, field2, field, [pfield],
                                             weight_field=weights[weight])
                            p.set_unit(pfield[1], unit_dict[pfield[1]])
                            p.set_unit(field2[1], unit_dict[field2[1]])
                            p.set_unit(field[1], unit_dict[field[1]])
                            p.set_log(field2[1], logs[field2[1]])
                            p.set_log(field[1], logs[field[1]])
                            p.set_xlim(extrema[field2[1]][0],
                                       extrema[field2[1]][1])
                            p.set_ylim(extrema[field[1]][0],
                                       extrema[field[1]][1])
                            p.set_zlim(pfield[1], extrema[pfield[1]][0],
                                       extrema[pfield[1]][1])
                            p.set_cmap(pfield, cmaps[pfield[1]])
                            if prof2d_annotate_time:
                                p.annotate_title("Time = %d Myr" % int(
                                    ds.current_time.in_units('Myr')))
                            p.save(make_file_name('2dProfs', field, w, ds,
                                                  weight=weight,
                                                  field2=field2,
                                                  pfield=pfield))
quit()
