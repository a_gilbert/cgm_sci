import cmocean

# user set params

test = True
msu_test = False
blue_test = False
blue = False

# cores per file

low_res = False
# if vanilla is true, must set kinetic or thermal to be true
vanilla = False
kinetic = False
thermal = False

crs = False
embra = False
lowz = False
massive = False
no_halo = False
starburst = False
zeus = False

galstat_cpf = 4

galcor_make_directories = True
galcor_write_extrema = True

join = True
keep = True
mail = True
mail_settings = 'abe'
address = 'agilbert39@gatech.edu'

make_slices = True
run_slices = True
slicer_annotate_time = True
slicer_cpf = 4
slicer_walltime = [1, 0, 0]

make_projections = True
run_projections = True
projector_annotate_time = True
projector_cpf = 4
projector_walltime = [1, 30, 0]

make_prof2d = True
run_prof2d = True
prof2d_annotate_time = True
prof2d_cpf = 4
prof2d_walltime = [2, 0, 0]

make_rprof = True
run_rprof = True
rprof_annotate_time = True
rprof_cpf = 4
rprof_walltime = [1, 0, 0]

make_hprof = True
run_hprof = True
hprof_annotate_time = True
hprof_cpf = 4
hprof_walltime = [1, 0, 0]

make_phase3d = True
run_phase3d = True
phase3d_add_radius = True
phase3d_annotate_time = True
phase3d_cpf = 4
phase3d_walltime = [14, 30, 0]

res = 128

theta = 30

phi = 45

max_r = (300, 'kpc')

r_vir = (200, 'kpc')

c = [0.5, 0.5, 0.5]

r_values = [0.1, 0.25, 0.5, 1, 1.5]

directions = ['x', 'z']

widths = {'radius1': (100, 'kpc'), 'radius2': (300, 'kpc')}

fields = [('gas', 'velocity_spherical_radius'), ('gas', 'density'),
          ('gas', 'entropy'), ('gas', 'metallicity'),
          ('gas', 'temperature'), ('gas', 'cooling_time'),
          ('gas', 'pmeasure'), ('gas', 'cell_mass')]

prof_fields = [('gas', 'cell_mass'), ('gas', 'metallicity'),
               ('gas', 'pmeasure')]

pfield_weights = dict(cell_mass=None,
                      metallicity='density',
                      pmeasure='density')

fields2 = [('gas', 'density'), ('index', 'spherical_radius'),
           ('gas', 'metallicity')]

weights = {"noWeight": None, "densityWeight": 'density', "cellMassWeight":
           'cell_mass'}

logs = dict(density=True,
            temperature=True,
            metallicity=True,
            velocity_magnitude=False,
            H_p0_fraction=True,
            entropy=True,
            spherical_radius=False,
            velocity_spherical_radius=False,
            cell_mass=True,
            cooling_time=True,
            pmeasure=True)

y_scales = dict(density='symlog',
                temperature='symlog',
                metallicity='symlog',
                velocity_magnitude='log',
                H_p0_fraction='symlog',
                entropy='symlog',
                spherical_radius='linear',
                velocity_spherical_radius='linear',
                cell_mass='symlog',
                cooling_time='log',
                pmeasure='log')

sym_dict = dict(velocity_spherical_radius=r"v_{r}",
                density=r"\rho",
                entropy=r"S",
                H_p0_fraction=r"n_{H}/n_{tot}",
                metallicity=r"Z",
                cell_mass=r"m_{cell}",
                temperature=r"T",
                smass=r"M_{\ast}",
                time=r"t",
                cooling_time=r"t_{cool}",
                pmeasure=r"t_{cool}/t_{ff}")

unit_dict = dict(velocity_spherical_radius="km/s",
                 density="g/cm**3",
                 entropy="cm**2*keV",
                 H_p0_fraction="dimensionless",
                 metallicity="Zsun",
                 cell_mass="Msun",
                 temperature='K',
                 smass='Msun',
                 time="yr",
                 spherical_radius='kpc',
                 cooling_time='yr',
                 pmeasure='dimensionless')

punit_dict = dict(velocity_spherical_radius="km**2/s",
                  density="g/cm**2",
                  entropy="cm**3*keV",
                  H_p0_fraction="cm",
                  metallicity="Zsun*cm",
                  cell_mass="Msun*cm",
                  temperature='K*cm',
                  cooling_time='yr*cm',
                  pmeasure='cm')

unit_labels = dict(velocity_spherical_radius="($km/s$)",
                   density="($g/cm^3$)",
                   entropy="($cm^2keV$)",
                   H_p0_fraction="",
                   metallicity="($Z_{\odot}$)",
                   spherical_radius='($kpc$)',
                   cell_mass="($M_{\odot}$)",
                   temperature='($K$)',
                   cooling_time='($yr$)',
                   pmeasure='')

cmaps = dict(velocity_spherical_radius=cmocean.cm.haline,
             density='viridis',
             entropy='Purples',
             H_p0_fraction=cmocean.cm.haline,
             metallicity='octarine',
             temperature='Reds_r',
             cooling_time=cmocean.cm.ice,
             pmeasure='RdBu',
             cell_mass='Greens')

pbase_names = {'slice': './plots/slices',
               'projs': './plots/projs',
               '1dProfsr': './plots/profs/1dprofs/radial',
               '1dProfsh': './plots/profs/1dprofs/height',
               '2dProfs': './plots/profs/2dprofs',
               '3dProfs': './plots/phase3d',
               'stats': './plots/stats'}


stat_file = "stat_data.pickle"

extrema_file = "extrema.pickle"

combo_file = 'combos.pickle'

comparison_reg = 'rtot'

if test:
    d_files = "../MW_Sim/DD????/DD????"
elif msu_test:
    d_files = "../../sims/isolated-galaxies/"
    d_files += "MW_1638kpcBox_800pcCGM_200pcDisk_lowres"
    d_files += "/DD????/DD????"
elif blue_test:
    d_files = "/u/sciteam/abg2/scratch/MW_1638kpcBox_800pcCGM_200pcDisk_lowres"
    d_files = d_files + '/DD000?/DD000?'
elif blue:
    if low_res:
        d_files = "/u/sciteam/abg2/scratch"
    else:
        d_files = "/scratch/sciteam/dsilvia/simulations/galaxy_simulation"
        d_files = d_files + "/reu_sims"
    d_files += "/MW_1638kpcBox_800pcCGM_200pcDisk"
    if low_res:
        d_files += '_lowres'
    elif vanilla:
        if kinetic:
            d_files += "/kineticFB"
        elif thermal:
            d_files += "/thermalFB"
    elif crs:
        d_files += "_CRs"
    elif embra:
        d_files += "_embraparticles"
    elif lowz:
        d_files += "_lowZHalo"
    elif no_halo:
        d_files += "_noHalo"
    elif starburst:
        d_files += "_starburst"
    elif zeus:
        d_files += "_ZEUSHydro"
    elif massive:
        d_files += "_massiveHalo"
    d_files = d_files + "/DD????/DD????"
