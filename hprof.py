"""
The goal of this script is to generate some general information
about the isolated galaxy on the MSU super computer. It creates
a profile plot of cylindrical height of each field in `fields`.
"""
import os.path
import yt
from params import (fields, c, widths, weights, logs,
                    unit_dict, max_r, hprof_cpf, hprof_annotate_time)
from utils import make_file_name, get_dfiles, get_extrema, process_data

yt.enable_parallelism()


extrema = get_extrema()
extrema['spherical_radius'] = (0, max_r[0])


d_series = yt.DatasetSeries(get_dfiles(), setup_function=process_data,
                            parallel=hprof_cpf)

for ds in d_series.piter():
    for w in widths.keys():
        sp = ds.sphere(c, widths[w])
        for field in fields:
            for weight in weights.keys():
                fn = make_file_name('1dProfsh', field, w, ds, weight=weight)
                if os.path.isfile(fn):
                    pass
                else:
                    plogs = {}
                    plogs[field[1]] = logs[field[1]]
                    p = yt.ProfilePlot(sp, 'disk_cylindrical_z_mag', field,
                                       weight_field=weights[weight],
                                       x_log=False,
                                       y_log=plogs)
                    p.set_unit('disk_cylindrical_z_mag', 'kpc')
                    p.set_unit(field[1], unit_dict[field[1]])
                    p.set_ylim(field, extrema[field[1]][0],
                               extrema[field[1]][1])
                    if hprof_annotate_time:
                        p.plots[field].axes.set_title("Time = %d Myr" % int(
                            ds.current_time.in_units('Myr')))
                        p.plots[field].axes.title.set_fontproperties(
                            p._font_properties)
                    p.save(fn)
quit()
