
#!/bin/bash
#PBS -l nodes=1:ppn=4
#PBS -l walltime=01:00:00
#PBS -N gal_cor
#PBS -e $PBS_O_WORKDIR/gal_cor.err
#PBS -o $PBS_O_WORKDIR/gal_cor.out
#PBS -m abe
#PBS -M agilbert39@gatech.edu

module swap PrgEnv-cray PrgEnv-gnu
export LD_LIBRARY_PATH=$HOME/software/anaconda3/lib:$LD_LIBRARY_PATH

source activate

cd $PBS_O_WORKDIR

aprun python galcor.py

exit 0
