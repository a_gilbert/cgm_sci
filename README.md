# cgm_sci
A number of scripts to conduct a parallelized analysis of the circumgalactic medium on a number of different simulations primarily found on bluewaters, but also made on Michigan State Universities hpcc system. The intend order of running is as follows:

1. Setup.py - generates all the directories and job scripts needed to conduct the full analysis.
2. galstat.py - generates statistical data used to create limits on all future plots. 
3. galcor.py - Examine the auto and cross correlation of fields of interest to the star formation rate. Also generates the extrema values for all future plots.
4. rprof.py, hprof.py, prof2d.py, phase3d.py, projector.py, slicer.py - The majority of the data analysis. 
