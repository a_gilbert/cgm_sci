"""
The goal of this script is to generate some general information
about the isolated galaxy on the MSU super computer. It creates
a projection plot of each of the fields in
`fields` at each key value of the dictionary `widths`.

For the projections, each version of the plot is also
generated with a number of different weights, which are given by the
key values of the dictionary `weights`.

Lastly, this script takes care of setting up the directories to
keep all of the plots generated organized according to the
following directory hierarchy:

     base_plot_type/width/field/field2/dir/weight
If a given variable is not applicable, it will not occur in
this hierarchy. So, for example, field2 and weight do not occur
in the path along which all slices are saved since both of those
variables play no role in generating slices. All plots are named
according to the string value of ds, a yt data object. This
will help in the generation of movies later on.

Notes:
------
The ability to create directories is not arbitrary in python on
unix systems. To enable this feature I followed the example of the
answer by user `dbw` on the stack overflow page
`https://stackoverflow.com/questions/5231901/permission-problems-when-creating-adir-with-os-makedirs-python`

I realize the length of the above line breaks pep8 standards, and apologize
profusely.
"""

import os.path
import yt
from params import (fields, directions, widths, weights,
                    projector_annotate_time, logs, unit_dict, punit_dict,
                    max_r, projector_cpf, cmaps)
from utils import process_data, get_extrema, get_dfiles, make_file_name

yt.enable_parallelism()

extrema = get_extrema()
extrema['spherical_radius'] = (0, max_r[0])

d_series = yt.DatasetSeries(get_dfiles(), setup_function=process_data,
                            parallel=projector_cpf)


for ds in d_series.piter():
    for w in widths.keys():
        for field in fields:
            for coord in directions:
                for weight in weights.keys():
                    fn = make_file_name('projs', field, w, ds, direction=coord,
                                        weight=weight)
                    if os.path.isfile(fn):
                        pass
                    else:
                        l = ds.unit_registry.lut['code_length'][0]
                        if weights[weight] is None:
                            zmin = extrema[field[1]][0] * l
                            zmax = extrema[field[1]][1] * l
                            u = punit_dict[field[1]]
                        else:
                            zmin = extrema[field[1]][0]
                            zmax = extrema[field[1]][1]
                            u = unit_dict[field[1]]
                        p = yt.ProjectionPlot(ds, coord, field,
                                              width=widths[w],
                                              weight_field=weights[weight])
                        p.set_log(field[1], logs[field[1]])
                        p.set_unit(field, u)
                        p.set_zlim(field, zmin, zmax)
                        p.set_cmap(field, cmaps[field[1]])
                        if projector_annotate_time:
                            p.annotate_timestamp(time_unit='Myr')
                            p.save(make_file_name('projs', field, w, ds,
                                                  direction=coord,
                                                  weight=weight))
quit()
