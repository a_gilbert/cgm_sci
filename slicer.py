"""
The goal of this script is to generate slice plots of all
fields specified in params.py by the user.

Lastly, this script takes care of setting up the directories to
keep all of the plots generated organized according to the
following directory hierarchy:

     base_plot_type/width/field/field2/dir/weight
If a given variable is not applicable, it will not occur in
this hierarchy. So, for example, field2 and weight do not occur
in the path along which all slices are saved since both of those
variables play no role in generating slices. All plots are named
according to the string value of ds, a yt data object. This
will help in the generation of movies later on.
"""
import os.path
import yt
from params import (fields, directions, widths, slicer_annotate_time,
                    logs, unit_dict, max_r, slicer_cpf, cmaps, make_slices)
from utils import (process_data, get_extrema, make_file_name, get_dfiles)

yt.enable_parallelism()

extrema = get_extrema()
extrema['spherical_radius'] = (0, max_r[0])

storage = {}

d_series = yt.DatasetSeries(get_dfiles(), setup_function=process_data,
                            parallel=slicer_cpf)

for ds in d_series.piter():
    if make_slices:
        for w in widths.keys():
            for field in fields:
                for coord in directions:
                    fn = make_file_name('slice', field, w, ds, direction=coord)
                    if os.path.isfile(fn):
                        pass
                    else:
                        s = yt.SlicePlot(ds, coord, field, width=widths[w])
                        s.set_unit(field, unit_dict[field[1]])
                        s.set_zlim(field, extrema[field[1]][0],
                                   extrema[field[1]][1])
                        s.set_log(field[1], logs[field[1]])
                        s.set_cmap(field, cmaps[field[1]])
                        if slicer_annotate_time:
                            s.annotate_timestamp(time_unit='Myr')
                        s.save(fn)
quit()
